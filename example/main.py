import pipeliner


handler = pipeliner.Handler()

#
# functions

def on_trigger(request):
    token = None
    if "X-Gitlab-Token" in request["headers"]: token = request["headers"]["X-Gitlab-Token"]
    branch = None
    if "ref" in request["args"]: branch = request["args"]["ref"].split("/")[2]
    user = None
    if "user_email" in request["args"]: user = request["args"]["user_email"]

    extra = {
        "branch": branch
    }
    if branch == "master":
        return pipeliner.TriggerEvent("deploy_test", token, extra, user)
    return None

def build(extra):
    print("Building")
    # call linux commands like this:
    pipeliner.shell("whoami")

def test(extra):
    print("Testing")

def deploy(extra):
    print("Deploying")

def do_after(extra):
    # send report email like this:
    handler.send_report_email()

#
# configure handler

handler.on_trigger(on_trigger)

#
# define pipelines

deploy_test = pipeliner.Pipeline("deploy_test")
deploy_test.define_extra(["branch"])
deploy_test.set_jobs([
    build,
    test,
    deploy
])
deploy_test.after(do_after)
handler.add_pipeline(deploy_test)
