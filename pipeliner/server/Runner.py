from collections import deque
from pipeliner.server.SqLiteDb import *
from pipeliner.utl.Capturing import *
import pipeliner.server.models as models
import threading
import traceback
import pipeliner.utl.tools as tools
import copy


class Runner:

    def __init__(self, server):
        self._server = server
        self._queue = deque()
        self._queue_lock = threading.Lock()
        self._queue_event = threading.Event()
        self._runner_thread = None
        self._running_log = None
        self._alive = False

    #
    # ==================== private methods ====================
    #

    def _runner_handler(self):
        while self._alive:
            event = self._pop_queue()
            if not event:
                self._wait_queue()
                continue

            self._run_pipeline(event)

    def _run_pipeline(self, event):
        # get pipeline to run
        pipeline = self._server.get_handler().get_pipeline(event.pipeline_name)
        if not pipeline: return

        # init database
        db = SqLiteDb()
        db.connect()

        try:
            # save pipeline start log to db
            pipeline_log = models.PipelineLogModel()
            pipeline_log.pipeline_name = pipeline.get_name()
            pipeline_log.user = event.user
            pipeline_log.start_time = tools.get_time()
            pipeline_log.save(db)
            Logger.log("Pipeline #%d has started executing" % pipeline_log.id, "run")

            jobs = copy.copy(pipeline.get_jobs())
            jobs.append(pipeline.get_after())
            self._notify_pipeline_start(pipeline_log, jobs)
            n_jobs = len(jobs)

            # execute jobs in the pipeline
            job_index = event.job_index
            job_failed = False
            exit_codes = []

            for i in range(0, n_jobs):
                is_after = i == n_jobs-1
                skip = job_failed or job_index is not None and job_index != i

                job = jobs[i]
                job_end_time = None
                exit_code = "r"

                # save job start log to db
                job_log = models.JobLogModel()
                job_log.id_pipeline_log = pipeline_log.id
                job_log.job_index = i
                job_log.job_name = "after" if is_after else job.__name__
                job_log.start_time = tools.get_time()
                job_log.exit_code = exit_code
                job_log.log = ""
                job_log.save(db)

                self._notify_job_start(job_log)

                if is_after or not skip:
                    try:
                        capturing = Capturing()
                        capturing.on_readline(self._on_readline(job_log))
                        capturing.start()
                        if is_after and not job:
                            exit_code = 0
                        else:
                            exit_code = job(event.extra)
                        capturing.stop()

                        job_end_time = tools.get_time()
                        if not exit_code: exit_code = 0
                        elif not isinstance(exit_code, int): exit_code = 1
                    except Exception as e:
                        job_log.log += traceback.format_exc(-1, e)
                        exit_code = "f"

                    if exit_code != 0: job_failed = True
                else:
                    exit_code = "s"
                exit_codes.append(exit_code)

                # save job log to database
                job_log.end_time = job_end_time
                job_log.exit_code = exit_code
                job_log.save(db)
                self._notify_job_end(job_log)
                Logger.log(
                    "Finished a job %d/%d name=%s exit_code=%s" % (i + 1, n_jobs, job_log.job_name, job_log.exit_code),
                    "run"
                )

            # find out pipeline exit code
            if exit_codes[-1] != 0:
                pipeline_log.exit_code = exit_codes[-1]
            else:
                pipeline_log.exit_code = exit_codes[0]
                for c in reversed(exit_codes[:-1]):
                    if c != "s":
                        pipeline_log.exit_code = c
                        break

            # save pipeline exit code and end timestamp
            pipeline_log.end_time = tools.get_time()
            pipeline_log.save(db)
            self._notify_pipeline_end()
        except Exception as e:
            traceback.print_exc()

        # close db
        db.close()

    def _on_readline(self, job_log):
        def decorate(msg):
            job_log.log += msg
            self._notify_job_log(job_log)
        return decorate

    def _wait_queue(self):
        self._queue_event.clear()
        self._queue_event.wait()

    def _pop_queue(self):
        with self._queue_lock:
            if self._queue: return self._queue.popleft()
            else: return None

    #
    # send email callback
    #

    def _func_send_report_email(self, email_from=None, subject=None, msg=None):
        try:
            server_name = self._server.get_server_name()
            pipeline_id = self._running_log["id"]
            url = self._server.get_public_url()+"/pipeline/%d" % (pipeline_id)

            if not self._running_log: return
            if email_from is None: email_from = self._running_log["user"]
            if not email_from: return
            if subject is None: subject = "%s | Pipeline #%s has finished." % (server_name, pipeline_id)

            txt = ''
            txt += '<p>%s has just finished executing pipeline #%s. Pipeline logs are available on the link below.</p>' % (server_name, pipeline_id)
            txt += '<p><a href="%s">%s</a></p>' % (url, url)
            if msg is not None:
                txt += "<br />"+msg

            self._server.get_mailer().send_email(email_from, subject, txt)
        except:
            return False

        return True

    def _func_get_users(self):
        users = self._server.get_config()["users"]
        public = []
        for u in users:
            public.append({
                "email": u["email"],
                "level": u["level"]
            })
        return public


    #
    # info about current running pipeline
    #

    def _notify_pipeline_start(self, pipeline_log, jobs):
        # prepare running log object
        job_logs = []
        for i in range(0, len(jobs)):
            is_after = i == len(jobs)-1
            log = {
                "job_index": i,
                "job_name": "after" if is_after else jobs[i].__name__,
                "start_time": None,
                "end_time": None,
                "exit_code": None,
                "log": ""
            }
            job_logs.append(log)

        self._running_log = {
            "id": pipeline_log.id,
            "pipeline_name": pipeline_log.pipeline_name,
            "user": pipeline_log.user,
            "start_time": pipeline_log.start_time,
            "end_time": pipeline_log.end_time,
            "exit_code": pipeline_log.exit_code,
            "jobs": job_logs
        }

    def _notify_pipeline_end(self):
        self._running_log = None

    def _notify_job_start(self, job_log):
        job = self._running_log["jobs"][job_log.job_index]
        job["start_time"] = job_log.start_time
        job["exit_code"] = job_log.exit_code

    def _notify_job_end(self, job_log):
        job = self._running_log["jobs"][job_log.job_index]
        job["end_time"] = job_log.end_time
        job["exit_code"] = job_log.exit_code

    def _notify_job_log(self, job_log):
        job = self._running_log["jobs"][job_log.job_index]
        job["log"] = job_log.log


    #
    # ==================== public methods ====================
    #

    def trigger(self, request):
        event = self._server.get_handler()._trigger(request)

        if event is None:
            raise Exception("Method on_trigger did not return a TriggerEvent object.")

        if not self._server.validate_token(event.token):
            raise Exception("Invalid token provided.")

        self.trigger_event(event, request["url"])
        return event

    def trigger_event(self, event, log_url="internal"):
        job_log = "all" if event.job_index is None else event.job_index
        Logger.log(
            "Received a trigger: pipeline=%s, job=%s trigger=%s" % (event.pipeline_name, job_log, log_url),
            "trigr"
        )

        pipeline = self._server.get_handler().get_pipeline(event.pipeline_name)
        if not pipeline: raise Exception("Pipeline does not exists.")

        with self._queue_lock:
            self._queue.append(event)
            self._queue_event.set()

    def get_queue_size(self):
        with self._queue_lock:
            return len(self._queue)

    def get_running_log(self):
        return self._running_log

    def start(self):
        self._alive = True
        self._queue.clear()
        self._queue_lock = threading.Lock()
        self._queue_event.clear()

        self._server.get_handler()._set_func("send_report_email", self._func_send_report_email)
        self._server.get_handler()._set_func("get_users", self._func_get_users)

        self._runner_thread = threading.Thread(target=self._runner_handler, args=())
        self._runner_thread.start()

    def stop(self):
        self._alive = False
        if not self._queue_event.is_set(): self._queue_event.set()
        self._runner_thread.join()
