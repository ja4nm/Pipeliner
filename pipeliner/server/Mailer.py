import smtplib, ssl
from email.message import EmailMessage


class Mailer:

    def __init__(self):
        self._con = None
        self._started = False
        self._server = None
        self._email = None
        self._password = None
        self._port = 465

    def _reconnect(self):
        code = -1
        try:
            code = self._con.noop()[0]
        except: pass

        if code != 250:
            context = ssl.create_default_context()
            self._con = smtplib.SMTP_SSL(self._server, self._port, context=context)
            self._con.login(self._email, self._password)

    def login(self, config):
        if "mailServer" not in config or config["mailServer"] is None:
            raise Exception("Config not present.")

        self._server = config["mailServer"]["server"]
        self._email = config["mailServer"]["email"]
        self._password = config["mailServer"]["password"]
        if "port" in config["mailServer"]:
            self._port = config["mailServer"]["port"]

        try:
            self._reconnect()
        except Exception as e:
            self.stop()
            raise e

    def send_email(self, emails_to, subject, email_msg):
        if self._con is None: raise Exception("Not connected.")
        self._reconnect()

        msg = EmailMessage()
        msg['Subject'] = subject
        msg['From'] = self._email
        msg['To'] = emails_to
        msg.add_header('Content-Type', 'text/html')
        msg.set_payload(email_msg)
        self._con.send_message(msg)

    def stop(self):
        if self._con:
            self._con.quit()
            self._con = None
