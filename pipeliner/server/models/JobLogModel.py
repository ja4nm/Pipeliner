import copy


class JobLogModel:

    def __init__(self):
        self.id = None
        self.id_pipeline_log = None
        self.job_index = None
        self.job_name = None
        self.start_time = None
        self.end_time = None
        self.exit_code = None
        self.log = None

    def save(self, db):
        obj = copy.copy(self.__dict__)

        if self.id is None:
            self.id = db.insert("job_logs", obj)
        else:
            del obj["id"]
            db.update("job_logs", "id = ?", [self.id], obj)

    @staticmethod
    def get_history(db):
        sql = "SELECT * FROM pipeline_logs ORDER BY pipeline_logs.start_time DESC"
        pipelines = db.query(sql)

        sql = '''
            SELECT job_logs.id, job_logs.id_pipeline_log, job_logs.exit_code
            FROM job_logs, pipeline_logs
            WHERE job_logs.id_pipeline_log = pipeline_logs.id
            ORDER BY job_logs.job_index ASC
        '''
        jobs = db.query(sql)

        pass
