import copy


class PipelineLogModel:

    def __init__(self):
        self.id = None
        self.pipeline_name = None
        self.user = None
        self.start_time = None
        self.end_time = None
        self.exit_code = None
        pass

    def save(self, db):
        obj = copy.copy(self.__dict__)

        if self.id is None:
            self.id = db.insert("pipeline_logs", obj)
        else:
            del obj["id"]
            db.update("pipeline_logs", "id = ?", [self.id], obj)
