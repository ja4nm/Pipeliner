/**
 * Class which shows info and logs of currently running pipeline.
 */

function PipelineLog() {
    this.refreshInterval = 2000;
    this.progressElm = null;
    this.logsElm = null;
    this.pipelineId = null;

    this._dateFormat = "DD.MM.YYYY HH:mm:ss";
    this._interval = null;
    this._onCompleteCb = null;
    this._onUpdateCb = null;
    this._queueSize = null;

    this._runningId = null;
}


/**
 * Initialise PipelineLog gui.
 */

PipelineLog.prototype.init = function (opt) {
    if (opt.refreshInterval) this.refreshInterval = opt.refreshInterval;
    if (opt.progressElm) this.progressElm = opt.progressElm;
    if (opt.logsElm) this.logsElm = opt.logsElm;
    if (opt.pipelineId) this.pipelineId = opt.pipelineId;

    if (this.pipelineId) {
        this._loadPipeline();
    } else {
        this._statUpdateInterval();
    }
};


/**
 * Load pipeline log from database.
 */


PipelineLog.prototype._loadPipeline = function(){
    var that = this;

    ajaxReq(
        "/ajax/pipeline-log",
        { id: this.pipelineId },
        function (r) {
            var log = r.log;
            if (!log) return;
            var finished = log.end_time != null;

            if (finished){
                that._loadProgressInfo(log);
                that._loadJobLogs(log);
            } else {
                that._statUpdateInterval();
            }
        }
    )
};

PipelineLog.prototype._loadProgressInfo = function(log) {
    if(!this.progressElm) return;
    this.progressElm.show();

    var id = log.id? log.id : this.pipelineId;
    var user = log.user? log.user : "/";
    var startTime = moment.unix(log.start_time/1000).format(this._dateFormat);
    var endTime = log.end_time? log.end_time : new Date().getTime();
    var duration = formatDuration((endTime-log.start_time)/1000);

    var html = '';
    html += '<div class="pipeline-list line">';
    html += '<div class="li">';
        html += '<ul class="pipeline-info">';
            html += '<li class="tight"><a href="'+PUBLIC_URL+'/pipeline/'+toStr(id)+'">#'+toStr(id)+'</a></li>';
            html += '<li><b>'+log.pipeline_name+'</b></li>';
            html += '<li><i class="fas fa-user"></i>'+user+'</li>';
            html += '<li><i class="fas fa-calendar-day"></i>'+startTime+'</li>';
            html += '<li><i class="fas fa-stopwatch"></i><span class="duration">'+duration+'</span></li>';
        html += '</ul>';
        html += '<ul class="pipeline-flow">';
            for(var i = 0; i < log.jobs.length; i++){
                var job = log.jobs[i];
                var color = exitCodeToColor(job.exit_code);
                var cls = (i === log.jobs.length-1)? "no-line" : "";
                html += '<li class="'+cls+'">'+job.job_name+'<span class="status '+color+'"></span></li>';
            }
        html += '</ul>';
    html += '</div>';
    html += '</div>';

    this.progressElm.html(html);
};

PipelineLog.prototype._loadJobLogs = function(log){
    if (!this.logsElm) return;
    this.logsElm.show();

    var html = '';
    html += '<div class="pipeline-log">';
    for (var i = 0; i < log.jobs.length; i++){
        var job = log.jobs[i];
        var color = exitCodeToColor(job.exit_code);
        var exitCodeStr = (job.exit_code !== null)? job.exit_code : "/";
        var cls = (i === log.jobs.length-1)? "alone" : "";

        var duration = "";
        if(job.start_time){
            var endTime = job.end_time? job.end_time : new Date().getTime();
            duration = formatDuration((endTime-job.start_time)/1000);
        }

        html += '<div class="log-item '+cls+'">';
            html += '<div class="title">';
            html += '<ul>';
                html += '<li><div class="pipeline-item">'+job.job_name+'<span class="status '+color+'"></span></div></li>';
                html += '<li><b>exit cdode: </b><span class="exit-code">'+exitCodeStr+'</span></li>';
                html += '<li><i class="fas fa-stopwatch"></i><span class="duration">'+duration+'</span></li>';
            html += '</ul>';
            html += '</div>';
            html += '<pre>'+toStr(job.log)+'</pre>';
        html += '</div>';
    }
    html += '</div>';

    this.logsElm.html(html);
};


/**
 * Start updating pipeline log in regular intervals.
 */

PipelineLog.prototype._updateLog = function () {
    var that = this;
    var includeLogs = (this.logsElm != null);

    ajaxReq(
        "/ajax/running-log",
        { includeLogs: includeLogs },
        function (r) {
            var log = r.log;
            var logId = log? log.id : null;
            that._queueSize = r.queue_size;

            if (that._runningId !== logId){
                // complete
                if (that._runningId !== null){
                    if (that.pipelineId) {
                        that._stopUpdateInterval();
                        that._loadPipeline();
                    }
                    if (that._onCompleteCb) that._onCompleteCb(that._queueSize);
                }
                // init
                that._runningId = logId;
                if (logId !== null){
                    that._loadProgressInfo(log);
                    that._loadJobLogs(log);
                }
            }
            // update
            else if (logId !== null) {
                that._updateProgressInfo(log);
                that._updateJobLogs(log);
            }

            if (that._onUpdateCb) that._onUpdateCb(log, that._queueSize);
        }
    )
};

PipelineLog.prototype._updateProgressInfo = function(log) {
    if(!this.progressElm) return;

    var endTime = log.end_time? log.end_time : new Date().getTime();
    var duration = formatDuration((endTime-log.start_time)/1000);
    this.progressElm.find(".duration").html(duration);

    for(var i = 0; i < log.jobs.length; i++){
        var job = log.jobs[i];
        var color = exitCodeToColor(job.exit_code);
        this.progressElm.find(".pipeline-flow li").eq(i).find(".status").attr("class", "status "+color);
    }
};

PipelineLog.prototype._updateJobLogs = function(log){
    if (!this.logsElm) return;

    for (var i = 0; i < log.jobs.length; i++){
        var job = log.jobs[i];
        var color = exitCodeToColor(job.exit_code);
        var duration = '';

        if(job.start_time){
            var endTime = job.end_time? job.end_time : new Date().getTime();
            duration = formatDuration((endTime-job.start_time)/1000);
        }
        var exitCodeStr = (job.exit_code !== null)? job.exit_code : "/";
        var elm = this.logsElm.find(".log-item").eq(i);
        elm.find(".status").attr("class", "status "+color);
        elm.find(".exit-code").html(exitCodeStr);
        elm.find(".duration").html(duration);

        var pre = elm.find("pre");
        var prevLen = (pre.html())? pre.html().length : 0;
        if (prevLen !== job.log.length) pre.html(job.log);
    }
};

PipelineLog.prototype._statUpdateInterval = function () {
    var that = this;
    this._updateLog();
    this._interval = setInterval(function () {
        that._updateLog();
    }, this.refreshInterval)
};

PipelineLog.prototype._stopUpdateInterval = function () {
    if(this._interval) clearInterval(this._interval);
};


/**
 * Public methods
 */

PipelineLog.prototype.onComplete = function(callback){
    this._onCompleteCb = callback;
};

PipelineLog.prototype.onUpdate = function(callback){
    this._onUpdateCb = callback;
};

PipelineLog.prototype.getQueueSize = function(callback){
    if(this._queueSize == null) this._updateLog();
    return this._queueSize;
};
