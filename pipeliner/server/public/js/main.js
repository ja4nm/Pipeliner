/**
 * Main javascript file
 */

function logout() {
    $("#logout-form").submit();
}

function ajaxReq(url, data, callback) {
    $.ajax({
        url: PUBLIC_URL+url,
        data: JSON.stringify(data),
        method: "POST",
        headers: {
            "X-Pipeliner-Csrf": _CSRF
        },

        contentType: "application/json; charset=utf-8",
        dataType: "json",

        success: function (r) {
            callback(r);
        }
    });
}

function toStr(str) {
    if (typeof str == "undefined" || str == null) return "";
    return String(str);
}


function formatDuration(dur_sec) {
    var date = new Date(dur_sec*1000);
    var dd = Math.floor(dur_sec/(24*60*60));
    var hh = date.getUTCHours();
    var mm = date.getUTCMinutes();
    var ss = date.getSeconds();

    if (dd > 0) return `${dd}d ${hh}h ${mm}m ${ss}s`;
    if (hh > 0) return `${hh}h ${mm}m ${ss}s`;
    if (mm > 0) return `${mm}m ${ss}s`;
    return `${ss}s`;
}

function exitCodeToColor(exitCode){
    exitCode = toStr(exitCode);
    if (exitCode === "0") return "green";
    if (!exitCode || exitCode === "s") return "";
    if (exitCode === "r") return "blue";
    return "red";
}
