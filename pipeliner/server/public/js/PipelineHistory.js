/**
 * Shows list of past run pipelines.
 */

function PipelineHistory() {
    this.listElm = null;
    this._dateFormat = "DD.MM.YYYY HH:mm:ss";
}


/**
 * Initialise PipelineHistory.
 */

PipelineHistory.prototype.init = function (opt) {
    if (opt.listElm) this.listElm = opt.listElm;

    this.updateHistory();
};

PipelineHistory.prototype.updateHistory = function () {
    var that = this;

    ajaxReq(
        "/ajax/history",
        null,
        function (r) {
            var history = r.history;
            that._loadHistory(history);
        }
    )
};

PipelineHistory.prototype.clearHistory = function(callback) {
    ajaxReq(
        "/ajax/clear-history",
        null,
        function (r) {
            if (callback) callback(r);
        }
    )
};

PipelineHistory.prototype._loadHistory = function (history) {
    var html = '';

    if (history && history.length > 0){
        html += '<div class="pipeline-list">';
        for (var i = 0; i < history.length; i++){
            var p = history[i];
            var user = (p.user)? p.user : "/";
            var startTime = moment.unix(p.start_time/1000).format(this._dateFormat);
            var duration = formatDuration((p.end_time-p.start_time)/1000);

            html += '<div class="li">';
            html += '<ul class="pipeline-info">';
                html += '<li class="tight"><a href="'+PUBLIC_URL+'/pipeline/'+p.id+'">#'+p.id+'</a></li>';
                html += '<li><b>'+p.pipeline_name+'</b></li>';
                html += '<li><i class="fas fa-user"></i>'+user+'</li>';
                html += '<li><i class="fas fa-calendar-day"></i>'+startTime+'</li>';
                html += '<li><i class="fas fa-stopwatch"></i>'+duration+'</li>';
            html += '</ul>';
            html += '<ul class="pipeline-flow">';
                for (var j = 0; j < p.jobs.length; j++){
                    var job = p.jobs[j];
                    var color = exitCodeToColor(job.exit_code);
                    var cls = (j === p.jobs.length-1)? "no-line" : "";
                    html += '<li class="'+cls+'">'+job.job_name+'<span class="status '+color+'"></span></li>';
                }
            html += '</ul>';
            html += '</div>';
        }
        html += '</div>';
    } else {
        html += '<div>Pipeline history is empty.</div>';
    }

    this.listElm.html(html);
};
