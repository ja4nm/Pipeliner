/**
 * Class for running defined pipelines.
 */

function PipelineRunner() {
    this.elm = null;
    this.pipelines = [];
    this.confirmFun = null;
    this._onAddCallback = null;
}


/**
 * Initialise PipelineRunner.
 */

PipelineRunner.prototype.init = function (opt) {
    if (opt.elm) this.elm = opt.elm;
    if (opt.confirmFun) this.confirmFun = opt.confirmFun;

    if (!this.elm || this.elm.length === 0) return;

    if (!this.confirmFun){
        this.confirmFun = function (p) {
            return confirm('Are you sure that you want to run pipeline "'+p.pipeline_name+'"?');
        };
    }

    this._initDefinedPipelines();
};

PipelineRunner.prototype._initDefinedPipelines = function () {
    var that = this;

    ajaxReq(
        "/ajax/defined-pipelines",
        null,
        function (r) {
            var pipelines = r.pipelines;
            that.pipelines = pipelines;

            if (pipelines) {
                that._loadDefinedPipelines(pipelines);
                that._initEvents();
            }
        }
    )
};

PipelineRunner.prototype._initEvents = function(){
    var that = this;

    // on run button click
    this.elm.find(".pipeline-flow li").click(function () {
        var btn = $(this);
        var index = btn.attr("data-pipeline-index");
        var pipeline = btn.closest("ul").attr("data-pipeline-name");
        var argInputs = btn.closest(".defined-pipeline").find(".param :text");
        if (index === "all") index = null;

        var args = [];
        argInputs.each(function () {
            args.push($(this).val());
        });

        that.runPipeline(pipeline, index, args);
    });
};

PipelineRunner.prototype._loadDefinedPipelines = function (pipelines) {
    var html = '';
    this.pipelines = {};

    if (pipelines.length > 0){
        for (var i = 0; i < pipelines.length; i++){
            var p = pipelines[i];
            this.pipelines[p.pipeline_name] = p;

            html += '<div class="defined-pipeline">';
            html += '<div class="pipeline-list-title">'+p.pipeline_name+'</div>';
            html += '<div class="pipeline-list no-line">';
            html += '<div class="li">';

            html += '<ul class="pipeline-flow clickable" data-pipeline-name="'+p.pipeline_name+'">';
                html += '<li class="no-line" data-pipeline-index="all">run<span class="status green"><i class="fas fa-play"></i></span></li>';
                for (var j = 0; j < p.jobs.length; j++){
                    var job = p.jobs[j];
                    var cls = (j === 0)? "no-line" : "";
                    html += '<li class="'+cls+'" data-pipeline-index="'+j+'">'+job+'<span class="status green"><i class="fas fa-play"></i></span></li>';
                }
            html += '</ul>';

            if (p.extra){
                html += '<div class="sep-10"></div>';
                for (var k = 0; k < p.extra.length; k++){
                    var arg = p.extra[k];
                    html += '<div class="param">';
                    html += '<div class="name">'+arg+'</div>';
                    html += '<input type="text" data-arg="'+arg+'" placeholder="'+arg+'" />';
                    html += '</div>';
                }
            }

            html += '</div>';
            html += '</div>';
            html += '</div>';
        }
    } else {
        html += '<div>There is no pipelines defined.</div>';
    }

    this.elm.html(html);
};

/**
 * Public methods.
 */

PipelineRunner.prototype.onAdd = function(callback){
    this._onAddCallback = callback;
};

PipelineRunner.prototype.runPipeline = function (pipeline, index, extra) {
    var that = this;
    var p = this.pipelines[pipeline];
    if (!p) return;

    var argInputs = this.elm.find('[data-pipeline-name="'+p.pipeline_name+'"]').closest(".defined-pipeline").find(".param");
    argInputs.removeClass("error");

    // validate extra args
    var error = false;
    var extraObj = {};

    for (var i = 0; i < p.extra.length; i++){
        if (!extra[i]) {
            argInputs.eq(i).addClass("error");
            error = true;
        } else {
          extraObj[p.extra[i]] = extra[i];
        }
    }
    if (error) return;

    // confirm run
    if (this.confirmFun){
        if (!this.confirmFun(p)) return;
    }

    ajaxReq(
        "/api/manual-trigger",
        {
            pipeline: p.pipeline_name,
            extra: extraObj,
            job_index: index,
        },
        function (r) {
            console.log(r);
            if (r.added_pipeline) argInputs.find(":text").val("");
            if (that._onAddCallback) that._onAddCallback(r);
        }
    );
};

