import sqlite3 as sqlite
import os


class SqLiteDb:

    _file = None

    '''
    SqLite database singleton class
    '''
    def __init__(self):
        self._connection = None
        pass

    @staticmethod
    def init(file):
        SqLiteDb._file = file
        if not os.path.exists(SqLiteDb._file):
            db = SqLiteDb()
            db.connect()
            db.create()
            db.close()


    def connect(self):
        self._connection = sqlite.connect(SqLiteDb._file)

    def close(self):
        self._connection.close()

    #
    # create methods
    #

    def create(self):
        cursor = self._connection.cursor()
        cursor.execute("PRAGMA foreign_keys = ON")
        self._table_logs(cursor)
        self._table_jobs(cursor)
        self._connection.commit()

    def _table_logs(self, cursor):
        sql = """
            CREATE TABLE IF NOT EXISTS pipeline_logs (
                id INTEGER PRIMARY KEY,
                pipeline_name VARCHAR NOT NULL,
                `user` VARCHAR,
                start_time INTEGER,
                end_time INTEGER,
                exit_code VARCHAR
            ) 
        """
        cursor.execute(sql)

    def _table_jobs(self, cursor):
        sql = """
            CREATE TABLE IF NOT EXISTS job_logs (
                id INTEGER PRIMARY KEY,
                id_pipeline_log INTEGER,
                job_index INTEGER NOT NULL,
                job_name VARCHAR,
                start_time INTEGER,
                end_time INTEGER,
                exit_code VARCHAR,
                log TEXT,
                
                FOREIGN KEY (id_pipeline_log)
                    REFERENCES pipeline_logs (id_pipeline_log)
                    ON DELETE CASCADE
                    ON UPDATE NO ACTION
            )
        """
        cursor.execute(sql)

    #
    # query methods
    #

    def commit(self):
        self._connection.commit()

    def insert(self, table, row):
        values = list(row.values())
        placeholders = ""
        columns = ""
        keys = list(row.keys())
        for i in range(0, len(keys)):
            if i > 0:
                columns += ", "
                placeholders += ", "
            columns += "`%s`" % keys[i]
            placeholders += "?"

        sql = "INSERT INTO `%s` (%s) VALUES (%s)" % (table, columns, placeholders)
        self._connection.execute(sql, values)
        self._connection.commit()
        id = self._connection.execute("SELECT last_insert_rowid()").fetchone()
        return id[0] if id else None

    def update(self, table, where, where_args, row):
        set_columns = ""
        keys = list(row.keys())
        for i in range(0, len(keys)):
            if i > 0: set_columns += ", "
            set_columns += "`%s` = ?" % keys[i]

        sql = "UPDATE `%s` SET %s WHERE %s" % (table, set_columns, where)
        args = list(row.values()) + where_args

        self._connection.execute(sql, args)
        self._connection.commit()

    def query(self, sql, args=None):
        if args is None: args = []
        cursor = self._connection.execute(sql, args)
        names = list(map(lambda x: x[0], cursor.description))
        rows = cursor.fetchall()
        lst = []
        for r in rows:
            obj = {}
            for i in range(0, len(names)):
                obj[names[i]] = r[i]
            lst.append(obj)
        return lst

    #
    # query methods
    #

    def _log_list(self, pipelines, jobs):
        job_map = {}
        for job in jobs:
            arr = job_map.get(job["id_pipeline_log"])
            if not arr:
                arr = []
                job_map[job["id_pipeline_log"]] = arr
            arr.append(job)

        for pipeline in pipelines:
            pipeline["jobs"] = job_map.get(pipeline["id"])

        return pipelines

    def get_history(self):
        sql = '''
            SELECT *
            FROM pipeline_logs
            WHERE pipeline_logs.end_time IS NOT NULL
            ORDER BY pipeline_logs.start_time DESC
        '''
        pipelines = self.query(sql)

        sql = '''
                    SELECT job_logs.id, job_logs.id_pipeline_log, job_logs.job_name, job_logs.exit_code
                    FROM job_logs, pipeline_logs
                    WHERE
                        job_logs.id_pipeline_log = pipeline_logs.id AND
                        pipeline_logs.end_time IS NOT NULL
                    ORDER BY job_logs.job_index ASC
                '''
        jobs = self.query(sql)

        return self._log_list(pipelines, jobs)

    def get_pipeline_log(self, id):
        sql = '''
                    SELECT *
                    FROM pipeline_logs
                    WHERE
                        pipeline_logs.id = ?
                '''
        pipelines = self.query(sql, [id])

        sql = '''
                    SELECT job_logs.*
                    FROM job_logs
                    WHERE
                        job_logs.id_pipeline_log = ?
                    ORDER BY job_logs.job_index ASC
                '''
        jobs = self.query(sql, [id])

        list = self._log_list(pipelines, jobs)
        return list[0] if len(list) > 0 else None

    def pipeline_log_exists(self, id):
        sql = '''
                SELECT 1
                FROM pipeline_logs
                WHERE
                    pipeline_logs.id = ?
            '''
        r = self.query(sql, [id])
        return len(r) > 0

    def clear_history(self):
        self._connection.execute("DELETE FROM job_logs")
        self._connection.execute("DELETE FROM pipeline_logs")
        self._connection.commit()
