from pipeliner.server.Server import *

if len(sys.argv) < 2:
    Logger.print("Usage: python -m pipeliner.server <path to config.json>")
    exit(0)

config_file = sys.argv[1]

server = Server()
try:
    server.init(config_file)
    server.start()
except Exception as e:
    Logger.log("Failed to start server: %s" % str(e), tag="err")
    exit(1)
