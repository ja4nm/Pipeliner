from pipeliner.server.Route import *
import flask
from flask import render_template
import pipeliner.utl.tools as tools
from pipeliner.server.SqLiteDb import *


class RouteLogin(Route):
    def route(self):
        self.protected(Permission.PUBLIC_ONLY)
        error = None

        # login form
        if flask.request.method == "POST":
            data = flask.request.form
            email = tools.get_def(data, "email")
            password = tools.get_def(data, "pass")
            user = self.server.get_user(email)

            ok = False
            if user is not None:
                ok = tools.checkpass(password, user["password"])
                if ok:
                    flask.session.clear()
                    flask.session["user"] = user["email"]
                    return flask.redirect(self.server.get_public_url()+flask.url_for("RouteHome"))
            if not ok:
                error = "Invalid email or password."

        return render_template("login.twig", error=error)


class RouteLogout(Route):
    def route(self):
        self.validate_csrf()
        data = flask.request.form
        logout = tools.get_def(data, "logout")
        if logout:
            flask.session.clear()
        return flask.redirect(self.server.get_public_url()+flask.url_for("RouteHome"))


class RouteHome(Route):
    def route(self):
        self.protected(Permission.GUEST)

        pipelines = self.server.get_defined_pipelines()
        queue_size = self.server.get_runner().get_queue_size()

        db = SqLiteDb()
        db.connect()
        history = db.get_history()
        db.close()

        return render_template("home.twig", defined_pipelines=pipelines, history=history, queue_size=queue_size)


class RoutePipeline(Route):
    def route(self, id):
        self.protected(Permission.GUEST)

        db = SqLiteDb()
        db.connect()
        exits = db.pipeline_log_exists(id)
        db.close()

        if not exits:
            return render_template("not_found.twig")
        return render_template("pipeline.twig", pipeline_id=id)

class RouteApiDocs(Route):
    def route(self):
        self.protected(Permission.GUEST)
        return render_template("api_docs.twig")
