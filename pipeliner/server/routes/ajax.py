from pipeliner.server.Route import *
from flask import jsonify
from pipeliner.server.SqLiteDb import *
import pipeliner.utl.tools as tools
import copy


class RouteRunningLog(Route):
    def route(self):
        self.validate_csrf()
        self.protected(Permission.GUEST, True)

        data = flask.request.get_json()
        include_logs = tools.get_def(data, "includeLogs")

        log = self.server.get_runner().get_running_log()
        queue_size = self.server.get_runner().get_queue_size()
        if log:
            log = copy.deepcopy(log)
            if not include_logs:
                for job in log["jobs"]:
                    job["log"] = ""

        r = jsonify({
            "log": log,
            "queue_size": queue_size
        })
        return r


class RoutePipelineLog(Route):
    def route(self):
        self.validate_csrf()
        self.protected(Permission.GUEST, True)

        data = flask.request.get_json()
        id = tools.parse_int(tools.get_def(data, "id"))

        db = SqLiteDb()
        db.connect()
        log = db.get_pipeline_log(id)
        db.close()

        r = jsonify({
            "log": log
        })
        return r


class RouteHistory(Route):
    def route(self):
        self.validate_csrf()
        self.protected(Permission.GUEST, True)

        db = SqLiteDb()
        db.connect()
        log = db.get_history()
        db.close()

        r = jsonify({
            "history": log
        })
        return r


class RouteClearHistory(Route):
    def route(self):
        self.validate_csrf()
        self.protected(Permission.ADMIN, True)

        running = self.server.get_runner().get_running_log()

        if running:
            r = jsonify({
                "error": "Can not clear history while a pipeline is running."
            })
        else:
            db = SqLiteDb()
            db.connect()
            db.clear_history()
            db.close()
            r = jsonify({
                "ok": "History cleared."
            })

        return r


class RouteDefinedPipelines(Route):
    def route(self):
        self.validate_csrf()
        self.protected(Permission.ADMIN, True)

        pipelines = self.server.get_defined_pipelines()

        tmp = []
        for p in pipelines:
            jobs = []
            for job in p.get_jobs():
                jobs.append(job.__name__)

            o = {
                "pipeline_name": p.get_name(),
                "jobs": jobs,
                "extra": p.get_defined_extra()
            }
            tmp.append(o)

        r = jsonify({
            "pipelines": tmp
        })
        return r
