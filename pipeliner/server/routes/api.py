from pipeliner.server.Route import *
from flask import request
from flask import jsonify
import pipeliner.utl.tools as tools
from pipeliner.utl.TriggerEvent import *


class RouteManualTrigger(Route):
    def route(self):
        self.protected(Permission.ADMIN, True, True)

        body = request.get_json()
        extra = tools.get_def(body, "extra")
        pipeline = tools.get_def(body, "pipeline")
        user = tools.get_logged_user()
        job_index = None
        try:
            job_index = int(tools.get_def(body, "job_index"))
        except: pass

        r = {}
        event = TriggerEvent(pipeline, None, extra, user, job_index)

        try:
            self.server.get_runner().trigger_event(event)
            r["added_pipeline"] = event.pipeline_name
        except Exception as e:
            r["error"] = str(e)

        r = jsonify(r)
        r.headers.add('Access-Control-Allow-Origin', '*')
        return r


class RouteTrigger(Route):
    def route(self):
        args = request.get_json()
        if not args: args = request.args
        headers = dict(request.headers)

        req_obj = {
            "url": request.url,
            "args": args,
            "headers": headers,
            "host": request.host
        }

        r = {}
        try:
            event = self.server.get_runner().trigger(req_obj)
            r["added_pipeline"] = event.pipeline_name
        except Exception as e:
            r["error"] = str(e)

        r = jsonify(r)
        r.headers.add('Access-Control-Allow-Origin', '*')
        return r
