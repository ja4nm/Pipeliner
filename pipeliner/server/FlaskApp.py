import flask
import pipeliner.server.routes as routes
import waitress
import pipeliner.utl.tools as tools
from datetime import datetime
import os


class FlaskApp:

    def __init__(self, server):
        self._flask = None
        self._server = server
        pass

    #
    # ==================== private methods ====================
    #

    def _before_request(self):
        if not "_csrf" in flask.session:
            flask.session["_csrf"] = tools.rand_str(64)

    def _context_processor(self):
        server_name = self._server.get_server_name()
        public_url = self._server.get_public_url()

        return dict(
            _csrf=flask.session["_csrf"],
            is_logged=self._is_logged,
            alert=self._alert,
            format_duration=self._format_duration,
            format_time=self._format_time,
            has_user_permission=self._has_user_permission,
            server_name=server_name,
            public_url=public_url
        )

    #
    # template functions
    #

    def _is_logged(self):
        return tools.get_logged_user() is not None

    def _has_user_permission(self, level):
        return self._server.has_user_permission(level)

    def _alert(self, msg):
        if not msg: return ""
        s = '<div class="alert">%s</div>' % (msg)
        return s

    def _format_duration(self, dur):
        dur_sec = int(dur/1000)

        days, remainder = divmod(dur_sec, 24*60*60)
        hours, remainder = divmod(remainder, 60*60)
        minutes, sec = divmod(remainder, 60)

        if days > 0: return "%dd %dh %dmin %ds" % (days, hours, minutes, sec)
        elif hours > 0: return "%dh %dmin %ds" % (hours, minutes, sec)
        elif minutes > 0: return "%dmin %ds" % (minutes, sec)
        else: return "%ds" % (sec)

    def _format_time(self, ts):
        ts = int(ts/1000)
        return datetime.fromtimestamp(ts).strftime("%d.%m.%Y %H:%M:%S")


    #
    # ==================== public methods ====================
    #

    def init(self):
        app = flask.Flask(__name__, template_folder="./views", static_folder="./public", static_url_path="")

        app.secret_key = "6zgd056lrHfUcEoERpw6mSt2MOyNbKcMWDpawR2h95m6AcV9q88tn7Rn8YwYlsMs"
        app.before_request(self._before_request)
        app.context_processor(self._context_processor)

        # api routes
        r = routes.api.RouteManualTrigger(self._server)
        app.add_url_rule('/api/manual-trigger', endpoint=r.get_name(), view_func=r.load, methods=["POST"])
        r = routes.api.RouteTrigger(self._server)
        app.add_url_rule('/api/trigger', endpoint=r.get_name(), view_func=r.load, methods=["POST"])

        # website routes
        r = routes.pages.RouteHome(self._server)
        app.add_url_rule('/', endpoint=r.get_name(), view_func=r.load, methods=["GET"])
        r = routes.pages.RouteApiDocs(self._server)
        app.add_url_rule('/api-docs', endpoint=r.get_name(), view_func=r.load, methods=["GET"])
        r = routes.pages.RouteLogin(self._server)
        app.add_url_rule('/login', endpoint=r.get_name(), view_func=r.load, methods=["GET", "POST"])
        r = routes.pages.RouteLogout(self._server)
        app.add_url_rule('/logout', endpoint=r.get_name(), view_func=r.load, methods=["POST"])
        r = routes.pages.RoutePipeline(self._server)
        app.add_url_rule('/pipeline/<id>', endpoint=r.get_name(), view_func=r.load, methods=["GET"])

        # ajax routes
        r = routes.ajax.RouteRunningLog(self._server)
        app.add_url_rule('/ajax/running-log', endpoint=r.get_name(), view_func=r.load, methods=["POST"])
        r = routes.ajax.RoutePipelineLog(self._server)
        app.add_url_rule('/ajax/pipeline-log', endpoint=r.get_name(), view_func=r.load, methods=["POST"])
        r = routes.ajax.RouteHistory(self._server)
        app.add_url_rule('/ajax/history', endpoint=r.get_name(), view_func=r.load, methods=["POST"])
        r = routes.ajax.RouteClearHistory(self._server)
        app.add_url_rule('/ajax/clear-history', endpoint=r.get_name(), view_func=r.load, methods=["POST"])
        r = routes.ajax.RouteDefinedPipelines(self._server)
        app.add_url_rule('/ajax/defined-pipelines', endpoint=r.get_name(), view_func=r.load, methods=["POST"])

        self._flask = app

    def get_app(self):
        return self._flask

    def serve(self, debug, host, port):
        if debug:
            self._flask.run(host, port, debug)
        else:
            waitress.serve(self._flask, host=host, port=port, threads=6, _quiet=True)
