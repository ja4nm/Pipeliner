
class OutsidePipelineError(RuntimeError):

    def __init__(self, method):
        super().__init__("OutsidePipelineError: method %s should only be called inside a pipeline." % method)
