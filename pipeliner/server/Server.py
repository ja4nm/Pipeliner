import json
from pipeliner.server.FlaskApp import *
from pipeliner.server.Runner import *
import pipeliner.utl.tools as tools
from pipeliner.server.SqLiteDb import *
from pipeliner.server.Mailer import *


class Server:

    def __init__(self):
        self.debug = False

        self._config = {}
        self._users = {}
        self._flask = None
        self._runner = None
        self._handler = None
        self._mailer = None
        self._tokens = set()
        self._public_url = None

    #
    # ==================== public methods ====================
    #

    def get_config(self):
        return self._config

    def get_runner(self):
        return self._runner

    def get_handler(self):
        return self._handler

    def get_user(self, email):
        if email in self._users: return self._users[email]
        else: return None

    def get_defined_pipelines(self):
        if self._handler: return self._handler.get_pipelines().values()
        else: return []

    def get_server_name(self):
        return self._config["name"]

    def get_mailer(self):
        return self._mailer

    def get_public_url(self):
        return self._public_url

    def validate_token(self, token):
        return token in self._tokens

    def has_user_permission(self, level):
        if level == "none": return True

        email = tools.get_logged_user()
        user = None
        if email: user = self.get_user(email)
        user_level = tools.get_def(user, "level")

        if level == "admin":
            if user_level != "admin":
                return False
        elif level == "guest":
            if user_level != "admin" and user_level != "guest":
                return False
        else:
            return False

        return True

    def init(self, config_file):
        with open(config_file) as f:
            self._config = json.load(f)

        self._users = {}
        for user in self._config["users"]:
            self._users[user["email"]] = user

        self._tokens.clear()
        for t in self._config["tokens"]: self._tokens.add(t)

        if self._config["publicUrl"] is None:
            self._public_url = ""
        else:
            self._public_url = self._config["publicUrl"].rstrip("/")

        Logger.log("Loaded config file: %s" % os.path.abspath(config_file), "init")

        handler = self._config["handler"]
        try:
            if not os.path.exists(handler):
                raise Exception("Handler file does not exists.")
            mod = tools.import_file(handler)
            self._handler = mod.handler
        except Exception as e:
            raise Exception("Failed to load handler file '%s': %s" % (os.path.abspath(handler), str(e)))

        self._flask = FlaskApp(self)
        self._flask.init()

    def start(self):
        SqLiteDb.init(self._config["database"])
        Logger.log("Initialised database: %s" % os.path.abspath(self._config["database"]), "init")

        self._mailer = Mailer()
        try:
            self._mailer.login(self._config)
            Logger.log("Mail server started", "init")
        except Exception as e:
            Logger.log("Mail server not started: %s" % e, "err")

        self._runner = Runner(self)
        self._runner.start()

        Logger.log(
            "Pipeliner server is now serving on http://%s:%d" % (self._config["host"], self._config["port"]),
            "init"
        )
        self._flask.serve(self.debug, self._config["host"], self._config["port"])

        self._runner.stop()
        self._mailer.stop()
        Logger.log("Pipeliner server stopped", "stop")
