import flask
from enum import Enum
import pipeliner.utl.tools as tools
from pipeliner.utl.Logger import *


class Permission(Enum):
    NONE = "none"
    PUBLIC_ONLY = "public_only"
    GUEST = "guest"
    ADMIN = "admin"


class RouteException(Exception):

    def __init__(self, code, msg, kill=False):
        super(RouteException, self).__init__(msg)
        self.code = code
        self.kill = kill


class Route:

    def __init__(self, server=None):
        self.server = server

    def protected(self, level=Permission.ADMIN, kill=False, allow_token=False):
        user = tools.get_logged_user()

        if allow_token:
            header_token = flask.request.headers.get("X-Pipeliner-Token")
            if header_token and self.server.validate_token(header_token): return

        if level == Permission.PUBLIC_ONLY:
            if user:
                raise RouteException("go_home", "User is already logged in.")
        else:
            if not self.server.has_user_permission(level.value):
                raise RouteException("unauthorized", "User is not authorized.", kill=kill)

    def validate_csrf(self):
        session_csrf = flask.session.get("_csrf")
        csrf = None

        try:
            csrf = flask.request.get_json()["_csrf"]
        except: pass
        if not csrf:
            try:
                csrf = flask.request.form["_csrf"]
            except: pass
        if not csrf:
            csrf = flask.request.headers.get("X-Pipeliner-Csrf")

        if session_csrf and session_csrf != csrf:
            raise RouteException("csrf", "Invalid CSRF token.")

    def load(self, **kwargs):
        try:
            return self.route(**kwargs)
        except RouteException as e:
            Logger.log("Route error on %s: %s" % (self.get_name(), str(e)), tag="err")
            if e.code == "unauthorized":
                if e.kill: return "", 401
                return flask.redirect(self.server.get_public_url()+flask.url_for("RouteLogin"))
            elif e.code == "go_home":
                return flask.redirect(self.server.get_public_url()+flask.url_for("RouteHome"))
            elif e.code == "csrf":
                return "", 401

    def route(self, **kwargs):
        pass

    def get_name(self):
        return self.__class__.__name__
