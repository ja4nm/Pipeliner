

class Pipeline:

    def __init__(self, name, defined_args=None, jobs=None, after=None):
        if defined_args is None: defined_args = []
        if jobs is None: jobs = []
        self._name = name
        self._jobs = jobs
        self._defined_args = defined_args
        self._after = after

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_jobs(self):
        return self._jobs

    def set_jobs(self, jobs):
        self._jobs = jobs

    def get_defined_extra(self):
        return self._defined_args

    def define_extra(self, args):
        self._defined_args = args

    def after(self, after_cb):
        self._after = after_cb

    def get_after(self):
        return self._after
