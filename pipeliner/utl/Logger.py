from datetime import datetime
import sys


class Logger:

    _stdout = sys.stdout
    _stderr = sys.stderr

    @staticmethod
    def print(msg, end="\n"):
        print(msg, file=Logger._stdout, end=end)

    @staticmethod
    def log(msg, tag="info"):
        print("%s [%s]\t: %s" % (Logger._time(), tag, msg), file=Logger._stdout)

    @staticmethod
    def err(msg, tag="err"):
        print("%s [%s]\t: %s" % (Logger._time(), tag, msg), file=Logger._stderr)

    @staticmethod
    def configure(stdout=None, stderr=None):
        if stdout: Logger._stdout = stdout
        if stderr: Logger._stderr = stderr


    @staticmethod
    def _time():
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S")
