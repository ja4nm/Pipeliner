from pipeliner.server.OutsidePipelineError import *


class Handler:

    def __init__(self):
        self._on_trigger_cb = None
        self._pipelines = {}
        self._func = {}
        pass

    def add_pipeline(self, pipeline):
        self._pipelines[pipeline.get_name()] = pipeline

    def get_pipelines(self):
        return self._pipelines

    def on_trigger(self, callback):
        self._on_trigger_cb = callback

    def get_pipeline(self, name):
        if name in self._pipelines: return self._pipelines[name]
        else: return None

    def send_report_email(self, emails_to=None, subject=None, msg=None):
        if "send_report_email" not in self._func: raise OutsidePipelineError("send_report_email")
        return self._func["send_report_email"](emails_to, subject, msg)

    def get_users(self):
        if "get_users" not in self._func: raise OutsidePipelineError("get_users")
        return self._func["get_users"]()

    #
    # backend methods
    #

    def _trigger(self, request):
        if self._on_trigger_cb: return self._on_trigger_cb(request)
        else: return None

    def _set_func(self, func, callback):
        self._func[func] = callback
