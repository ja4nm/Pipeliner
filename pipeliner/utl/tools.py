import importlib.util
import bcrypt
import random
import string
import time
import flask
import subprocess


'''
Hash or check password using bcrypt
'''
def hashpass(raw_pass):
    return bcrypt.hashpw(raw_pass.encode("utf-8"), bcrypt.gensalt())

def checkpass(raw_pass, hash):
    return bcrypt.checkpw(raw_pass.encode("utf-8"), hash.encode("utf-8"))

'''
Dinamcly import module form file path
'''
def import_file(file):
    spec = importlib.util.spec_from_file_location("*", file)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    return mod

'''
Try parse int, on fail return default value
'''
def parse_int(string, default=None):
    x = default
    try:
        x = int(string)
    except: pass
    return x

'''
Generate random string using [a-zA-Z0-9]
'''
def rand_str(len=8):
    s = string.ascii_letters + string.digits
    return ''.join((random.choice(s) for i in range(len)))

'''
Get key or return default value.
'''
def get_def(obj, key, default=None):
    if obj and key in obj: return obj[key]
    else: return default

'''
Get time in milliseconds
'''
def get_time():
    return int(time.time()*1000)

'''
Returns logged user, if not logged, null is returned.
'''
def get_logged_user():
    return flask.session.get("user")

'''
Execute shell command
'''
def shell(command):
    proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while True:
        line = proc.stdout.readline()
        if not line: break
        print(line.decode("utf-8"), end="")
        line.rstrip()

    return proc.poll()
