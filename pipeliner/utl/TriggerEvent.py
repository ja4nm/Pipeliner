
class TriggerEvent:

    def __init__(self, pipeline_name, token, extra=None, user=None, job_index=None):
        self.pipeline_name = pipeline_name
        self.token = token
        self.extra = extra
        self.user = user
        self.job_index = job_index
