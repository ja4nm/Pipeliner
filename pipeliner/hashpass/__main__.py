#
# Tool generating hashed passwords which are used in config.json.
#
from pipeliner.utl import tools
import sys
import getpass

if len(sys.argv) < 1:
    print("Usage: python -m pipeliner.hashpass")
    exit(0)

try:
    raw_pass = getpass.getpass("password: ")
    print(tools.hashpass(raw_pass).decode("utf-8"))
except:
    print("")
    exit(1)
