# Pipeliner

Lightweight and highly customizable python based CI/CD server.

Jan M, Jun 2020

Supported features:

- Define your pipelines using python
- Call shell commands from python
- Execute pipelines on GitHub/Gitlab webhooks (or any other custom post request)
- User friendly web interface for triggering pipelines manually and reading logs
- Sending report emails after a pipeline has finished
- Guest (read-only) access to web user interface


## Getting started

Lets provide a simple example on how to install and set up you pipeliner server. First, let's create a directory
/opt/pipeliner/Pipeliner and clone this repo in it.

```
mkdir -p /opt/pipeliner/Pipeliner
cd /opt/pipeliner/
git clone https://gitlab.com/ja4nm/Pipeliner.git ./Pipeliner
```

Install the python package using the command below.

```
python3 -m pip install ./Pipeliner
```

We will store our pipelines in /home/user/pipelines folder, so let's copy Pipeliner/example/main.py to it.
```
mkdir -p /home/user/pipelines
cp Pipeliner/example/main.py /home/user/pipelines/main.py
```

Let us make a copy of Pipeliner/sample.config.json and change the settings in it.

We have changed the server name, tokens array and added an admin user user.demo@gmail.com. All passwords in config.json 
must be hashed with bcrypt. You can use `pipeliner.hashpass` module to create hashed passwords as shown below. 
If you want, you can also add another user with `"level": "guest"` which would have read-only access. 

We have also set the `handler` to point to main.py in our pipelines directory and set the public url of the application 
to http://pipelinerdemo.si/pipeliner as we are gonna use nginx to serve it publicly later on.

Pipeliner server keeps logs in a MySQLite database, which we are going to store in /opt/pipeliner/pipeliner.db file.

Mail server setting are optional. If you do not want to use it, set `mailServer.host` to null. However, if you do want 
to use it, you can use Gmail's smtp server (https://www.androidauthority.com/gmail-smtp-settings-801100/), but I
recommend you to generate an App password, to be more secure.

```
cp ./Pipeliner/sample.config.json config.json
nano config.json

{
  "name": "Pipeliner demo",
  "host": "127.0.0.1",
  "port": 9000,
  "publicUrl": "http://pipelinerdemo.si/pipeliner",

  "handler": "/home/user/pipelines/main.py",
  "database": "/opt/pipeliner/pipeliner.db",

  "mailServer": {
    "server": "smtp.gmail.com",
    "port": null,
    "email": "user.demo@gmail.com",
    "password": "qmwmvihdktvpndgc"
  },

  "tokens": [
    "v9yufUIjkRDFRkfX8ciP1gaWQN5XfCrzdMfGBajwjqKJze81KxewVaqZiky2eFzY"
  ],

  "users": [
    {
      "email": "user.demo@gmail.com",
      "password": "$2b$12$6PGXovlmANxZInpD/FjVbe7qKmd1rF95R.m.d8fNpaJFuZhtGiq9e",
      "level": "admin"
    }
  ]
}
```

Create a bcrypt password hash for user.demo@gmail.com:
```
python3 -m pipeliner.hashpass
password: <your password>
```

You can now start the Pipeliner server using a command below. If you navigate to http://127.0.0.1:9000, you should be
able to log in to the Pipeliner web interface.
```
python3 -m pipeliner.server /opt/pipeliner/config.json
```

## Nginx configuration

We are going to run our server as systemd service and serve it using Nginx server. I am gonna assume you are familiar
with systemd services and nginx, so this section will be short. 

Make a systemd service file in `/etc/systemd/system/pipeliner.service` and then enable and start the service.
```
[Unit]
Description=Pipeliner server
After=network.target
StartLimitIntervalSec=0

[Service]
WorkingDirectory=/home/user/pipelines/
ExecStart=/usr/bin/python3 -m pipeliner.server /opt/pipeliner/config.json
TimeoutSec=60
Type=simple
User=user
Group=user

[Install]
WantedBy=multi-user.target
```

```
sudo systemctl enable pipeliner
sudo systemctl start pipeliner
```

Add the following lines to you Nginx configuration and then restart the Nginx server. You can also enable SSL encryption
at this point, but we are not going to bother. 
```
server {
    listen 80;
    server_name pipelinerdemo.si;

    location ^~ /pipeliner {
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://localhost:9000/;
    }
}
```
```
sudo systemctl restart nginx
```

You should now be able to access the Pipeliner web interface through http://pipelinerdemo.si/pipeliner

## Sample pipeline

If you open the file /home/user/pipelines/main.py, you can see that we already have a sample pipeline defined. Right
now, this pipeline is just printing stuff, but keep in mind, that you can implement whatever logic you want.

You can define a pipeline by creating a pipeliner.Pipeline object and then adding it to you handler using 
`handler.add_pipeline(pipeline)` method.

Each pipeline has a:
- unique name
- list of jobs (a.k.a. stages / functions to run)
- list of names of possible extra arguments (in the web interface there will be an input filed for each extra argument you define)
- after callback

Each job inside a pipeline is executed sequentially and should return an exit code. If None is returned, an
exit code of 0 is assumed. If a job returns a non 0 exit code, then all the later jobs are skipped.

The after callback is executed after every job in a pipeline has finished, regardless of their exit codes.

The `handler.on_request(request)` callback is called whenever a request to http://pipelinerdemo.si/pipeliner/api/trigger
is made. This callback should return a TriggerEvent object with information of which pipeline to run.

```
TriggerEvent(pipeline_name, token, extra=None, user=None, job_index=None)

pipeline_name: which pipeline to run
token: token to validate the TriggerEvent (get it form the request argument)
extra: extra paramters, which are passed to each job in the pipeline
user: email of a user who is executing the pipepline (get it form the request argument)
job_index: which job inside a pipeline to run (if None, run all)
```

If you want to execute a shell command inside a job, you can do it with a `pipeliner.shell()` function.

If you want to send a report email, you can use a `handler.send_report_email()` function. By default this function sends
an email to the user of TriggerEvent. You can get a list of all users defined in config.json by calling
`handler.get_users()` method.

After you update your handler file (main.py), you should restart the Pipeliner server:
```
sudo systemctl restart pipeliner
```

## Gitlab webhook example

In your GitLab project navigate to `settings -> webhooks` and add the url http://pipelinerdemo.si/pipeliner/api/trigger
In the field "Secret Token" enter a token from you config.json file. If you now push to your repo, the pipeline
"deploy_test" should be executed automatically.

## API docs

http://127.0.0.1:9000/api/trigger

Trigger the execution of a pipeline. Configure your git webhook to point to this url and make sure to include a token from config.json in request body or headers. Request to his endpoint will execute on_trigger callback in your pipeline handler and will pass the request body and headers to the callback argument.
```
METHOD: POST
```

http://127.0.0.1:9000/api/manual-trigger

Trigger the execution of a pipeline manually. You can specify jobIndex to run a single job inside a pipeline. If set to null, the whole pipeline will be executed. You can also specify arbitrary extra arguments to be passed to a pipeline. Make sure to include a token form config.json in X-Pipeliner-Token header.
```
METHOD: POST
HEADERS:
    "X-Pipeliner-Token": "token from config.json"
BODY:
    {
        "extra": {
            "branch": "master",
            ...
        },
        "job_index": 1,
        "pipeline": "pipelineName"
    }
```

## Screenshots

<a href="http://janm.si/other/pipeliner/pipeliner01.png"><img src="http://janm.si/other/pipeliner/pipeliner01.png" height="200" /></a>
<a href="http://janm.si/other/pipeliner/pipeliner02.png"><img src="http://janm.si/other/pipeliner/pipeliner02.png" height="200" /></a>


---
By Jan M
