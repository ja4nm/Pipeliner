import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

requirements = [
    "flask==1.1.2",
    "waitress==1.4.4",
    "bcrypt==3.1.7"
]

setuptools.setup(
    name="pipeliner-janm",
    version="1.0.0",
    author="Jan Mikolic",
    author_email="mikolicjan@gmail.com",
    description="Lightweght CI/CD server.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://janm.si",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
